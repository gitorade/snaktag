#!/usr/bin/env python3
# coding: utf-8
#
# Author        : Matthias Stauber
# Created on    : 22.02.2020
# Last modified : 22.02.2020
#
# Classify websites as "contains_ranking" or "does_not_contain_ranking" (binary classification)
#
# Possible features include:
#     - URL
#     - Title
#     - Certain keywords / context words
#     - DOM structure / path
#     - Format (italic, ...)
#     - CSS classes / layout
#     - Surround elements
#


# Bayes classifier Multinomial Naive Bayes
# CountVectorizer

# Imbalanced Data:
#     - https://machinelearningmastery.com/tactics-to-combat-imbalanced-classes-in-your-machine-learning-dataset/
#     - https://towardsdatascience.com/handling-imbalanced-datasets-in-deep-learning-f48407a0e758?gi=9c3bf63925b4
#     - https://elitedatascience.com/imbalanced-classes
#     - https://github.com/scikit-learn-contrib/imbalanced-learn

"""
Extract text from

div.h3(.a)

<a>
<td>
<li>
<p>
<hX>
<strong>

check product name from database

Paper: remove everything but high level <div> (which should contain content)
word frequencies in blocks
text size, bold characters, italic, captions, titles, --> sentence score
common words title and sentence
"""


"""
bs4.element.NavigableString
    - call unicode() when need only the string
bs4.element.tag

"""
import os
import sys
from pathlib import Path
sys.path.insert(0, Path(os.path.join(os.getcwd(), '../modules')).resolve())

import requests
import numpy as np
import time
import string
import pandas as pd
import tensorflow as tf
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
import bs4.element
import re
from joblib import Parallel, delayed
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import precision_recall_curve, accuracy_score
from sklearn.metrics import plot_confusion_matrix
import multiprocessing
#twisted

from http_helpers import RequestCache, DEFAULT_HTTP_HEADERS_2

CACHE_DIR = '../../data/.requests_cache'

TITLE_KEYWORDS_BILINGUAL = [
    'best',
    'test',
    'vergleich',
    'compar',
    'top',
    'rank'
]

KEYWORDS_GERMAN = [
    'produkt',
    'note',
    'kategori',
    'stärke',
    'schwäche',
    'pro',
    'contra',
    'vorteil',
    'meinung',
    'vergleich',
    'test',
    'top',
    'rang',
    'sehr',
    'gut',
    'befriedrig',
    'ausreichend',
    'schlecht',
    'beste',
    'kriteri',
    'kauf',
    'platz',
    'ratgeber',
    'artikel',
    'preis',
    'verkauf',
    'kunde',
    'teuer',
    'billig',
    'günstig',
    '1',
    '2',
    '3',
    '4',
    '5',
    'sieger',
    'verlierer'
    
]

# t.b.d.
KEDYWORDS_ENGLISH = [
    'product',
    'rank',
    'best',
    'worst',
    'feature',
    'deal',
    'user',
    'trial',
    'free',
    'top'
]

def trivial_classifier(soup):
    pass
    
def scrape_title(bsobj):
    # Check for <head> first
    head = bsobj.head
    if not head:
        return ''
    
    # Find <title> in <head>
    title = head.title
    if not title:
        return ''
    
    return title.text

def is_text(bsobj):
    """ bs4 provides .stripped_strings, but we cannot filter out unwanted tags through the returned
        str objects """
    if not bsobj or bsobj.isspace():
        return False

    if type(bsobj) is not bs4.element.NavigableString:
        return False
    
    # Disabled: Warum nicht auch hier nach keywords suchen ?!
    #if bsobj.parent.name in ['script', 'style']:
    #    return False

    return True

def scrape_readable_text(bsobj):
    textTags = bsobj(text=True)
    textTags = filter(is_text, textTags)
    return '\n'.join([s.strip() for s in textTags])    

def extract_features_from_html(soup, url):
    """
        Feature row:
            0: <title> contains at least one of TITLE_KEYWORDS_BILINGUAL
    """
    
    #title = scrape_title(soup).lower()
    # Keep only letters (including German Alphabet), digits and whitespace
    #pattern = re.compile(r'[^a-zäöüß0-9\s]+', re.UNICODE)
    #title = pattern.sub('', title)
    # *** Hier lohnt es sich evtl. die performanteste Methode zu messen ***
    # https://stackoverflow.com/questions/3271478/check-list-of-words-in-another-string

    # Findet aktuell jegliche Überlappungen
    #keywordInTitle = any(x in title for x in TITLE_KEYWORDS_BILINGUAL)
    
    # Simple approach: Look for keywords only without any structural information etc.
    # Any matching substring counts as True (this makes matching [1,2,3,4,5] useless probably)
    text = scrape_readable_text(soup).lower()
    
    return list(map(int, [x in text for x in KEYWORDS_GERMAN]))
        
def work(urlDict, URLs):
    for x in URLs:
        soup = BeautifulSoup(urlDict[x].text, 'lxml')
        title = scrape_title(soup).lower()


def Workbench_extract_features_from_HTML(htmlText: str, srcURI: str):
    """
        Example function to extract single feature row for one webpage.
        The webpage is already pre-fetched from the given URI.
        Different feature extraction methods may be used within the same workbench.
    """
    
    return []

def extract_features_1_single(URLs):
    count = 0   
    features = []
    start = time.time()
    for x in URLs:
        response = rcache.get(x)
        if response.status_code != requests.codes.ok:
            print("Failed to fetch URL: '{}' (code: {})".format(x, response.status_code))
            continue
        soup = BeautifulSoup(response.text, 'lxml')
        features.append(extract_features_from_html(soup, x))
        count += 1
    X = pd.DataFrame(features, columns=KEYWORDS_GERMAN)

def extract_features_1_parallel(URLs):
    """ This implementation assumes the pages are stored offline. 
        (else, there would hardly be a performance gain due to network bottlenecks) """
        
    rcache = RequestCache(cachePath=CACHE_DIR)
    sites = rcache.get_underlying_dict()
    
    numThreads = 8 # multiprocessing.cpu_count()
    URLs = URLs[:1000]
    chunks = np.array_split(URLs, numThreads)
    
    # one thread looks up site HTML text from dict and pushes to queue
    # another thread runs HTML parser and extracts features?
    # RAM speed bound??
    
    URLs = [x for x in URLs if x in sites]
            
    print('Initializing parallel execution on {} threads...'.format(numThreads))
    
    start = time.time()
    with multiprocessing.Manager() as manager:
        urlDict = manager.dict(sites)
        jobs = []
        for i in range(numThreads):
            jobs.append(multiprocessing.Process(target=work, args=(urlDict, chunks[i])))
            jobs[i].start()
            
        for i in range(numThreads):
            jobs[i].join()
    stop = time.time()
    print('Finished in {}s'.format(stop - start))

def extract_features_1(URLs):
    """ Expects a list of URLs """
    rcache = RequestCache(cachePath=CACHE_DIR)
    count = 0   
    features = []
    start = time.time()
    for x in URLs:
        response = rcache.get(x)
        # We must somehow adapt the output vector, too (initially contains every dataset)
        if response.status_code != requests.codes.ok:
            print("Failed to fetch URL: '{}' (code: {})".format(x, response.status_code))
            continue
        soup = BeautifulSoup(response.text, 'lxml')
        features.append(extract_features_from_html(soup, x))
        count += 1
    X = pd.DataFrame(features, columns=KEYWORDS_GERMAN)
    print('Finished in {}s'.format(time.time() - start))
    
    rcache.store(useCompression=False)
    rcache.store()
    return X

def load_dataset_1():
    FILE = '../../data/mein.csv'
    sites = pd.read_csv(FILE, sep=';', header=None)
    # remove row number column (implicit)
    sites = sites.iloc[:, 1:]
    # Last column == 4 --> isRanking
    sites.iloc[:, -1] = (sites.iloc[:, -1] == 4).astype(int)
    return sites

def load_dataset_2():
    FILE = '../../data/url_dataset.csv'
    sites = pd.read_csv(FILE)
    return sites

def clean_dataset(df):
    """ Expects two columns [fullURL:str, isRanking:int] """
    
    print('Number of rows:')
    print('    base dataset                : {}'.format(len(df)))
    
    # Remove all rows with missing values
    df.replace('', np.nan, inplace=True)
    df.dropna(inplace=True)
    print('    dropna                      : {}'.format(len(df)))
    
    # Remove all rows with invalid URL (primitive check)
    df = df[df.iloc[:, 0].str.startswith('http')]
    print('    removing invalid URLs       : {}'.format(len(df)))
    
    # Remove all rows with invalid output column
    df = df[df.iloc[:, 1].isin([0, 1])]
    print('    removing invalid categories : {}'.format(len(df)))
    
    # Remove duplicates
    df.drop_duplicates()
    print('    removing duplicate entries  : {}'.format(len(df)))
    
    return df

# TODO: Use pre-defined library function, e.g. from sklearn ?
def select_balanced_samples(df, numSamples):
    """
        Randomly select subset of data with equal number of both classes
        (given binary classification).
    """
    # First shuffle dataframe (alternative: Use random indices)
    df = df.sample(frac=1)
    
    # Select half from first class
    subset = df[df.iloc[:, 1] == 0].iloc[:numSamples//2, :]
    
    # Select half of second class, ensuring size always equals numSamples
    subset = subset.append(df[df.iloc[:, 1] != 0].iloc[:(numSamples + 1)//2, :])
    
    # Shuffle adjacest classes and index from 0..N
    return subset.sample(frac=1).reset_index(drop=True)    

def build_dataset_from_URLs(sitelist):
    # sklearn.utils.shuffle
    # df.sample().reset_index(drop=True)
    
    # Use subset of dataset (10000 is quite large)
    # Make sure classes are balanced (50:50 Yes:No [is_ranking])
    sitelist = select_balanced_samples(sitelist, numSamples=1000)
    
    # Labels (output) are already known in last column [0, 1]
    y = sitelist.iloc[:, -1]
    # NOTE: For large datasets, using asynchronous network requests may be faster
    
    # We must extract the relevant features from our websites HTML first:
    X = extract_features_1(sitelist.iloc[:, 0])
    
    dataset = X.assign(is_ranking=y)
    FEATURES_DATAFILE = '../../data/mein_features_2.csv'
    dataset.to_csv(FEATURES_DATAFILE, index=False)

def train_and_evaluate_model(dataset):
    dataset = dataset.astype(np.float)
    X = dataset.iloc[:, :-1].values
    y = dataset.iloc[:, -1].values
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)
    
    # Bayes classifier Multinomial Naive Bayes
    classifier = MultinomialNB()
    classifier.fit(X_train, y_train) # theoretically supports sample weights
    
    display = plot_confusion_matrix(classifier, X_test, y_test, cmap='cividis')
    plt.title('Confusion Matrix for "is_ranking"')
    print('Accuracy: {}%'.format(100*accuracy_score(y_test, classifier.predict(X_test))))
    
def main():
    # trainingSites = clean_dataset(load_dataset_1())
    # build_dataset_from_URLs(trainingSites)
    
    dataset = pd.read_csv('../../data/mein_features_2.csv')
    train_and_evaluate_model(dataset)

if __name__ == '__main__':
    main()