# confirm TensorFlow sees the GPU
import tensorflow as tf
from tensorflow.python.client import device_lib

if 'GPU' in str(device_lib.list_local_devices()):
	print('\n===  TensorFlow GPU OK  ===\n')

print("\n===  Num GPUs Available: {}  ===\n".format(len(tf.config.experimental.list_physical_devices('GPU'))))

# confirm Keras sees the GPU
from keras import backend
if len(backend.tensorflow_backend._get_available_gpus()) > 0:
	print('\n===  Keras GPU OK  ===\n')
