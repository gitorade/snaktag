#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
import requests
import pickle
import time
import random
import lzma
import lz4.frame
import gzip
from urllib.parse import urlsplit

###################################################################################################
# Spoof browser HTTP headers.
###################################################################################################
DEFAULT_HTTP_HEADERS = {
    "User-Agent"                : "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0",
    "Accept"                    : "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Encoding"            : "gzip, deflate, br",
    "Accept-Language"            : "de,en-US;q=0.7,en;q=0.3",
    "Connection"                : "keep-alive",
    "DNT"                        : "1",
    "Upgrade-Insecure-Requests" : "1"
}

DEFAULT_HTTP_HEADERS_2 = {
    'User-Agent'                : 'Mozilla/5.0 (Windows NT 10.0; rv:68.0) Gecko/20100101 Firefox/68.0',
    'Accept'                    : 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Language'           : 'en-US,en;q=0.5',
    'Accept-Encoding'           : 'gzip, deflate, br',
    'DNT'                       : '1',
    'Connection'                : 'keep-alive',
    'Upgrade-Insecure-Requests' : '1'
}

###################################################################################################
# Utility functions
###################################################################################################
def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    else:
        return text

def get_host(url):
    host = "{0.netloc}".format(urlsplit(url))
    return remove_prefix(host, 'www.')

###################################################################################################
# Reduce actual network requests made while testing web scraping code
# supposed to be a drop-in replacement for "requests.get(...)" calls
#
# Cached entries are persistent and clearing the cache must be manually managed.
###################################################################################################
class RequestCache:
    def __init__(self, cachePath='.requests_cache', compressed=True):
        self.cachePath = cachePath
        self.rcache = {}              # Cache the responses for requests.get(URL)
        self.last_requests = {}          # Timestamp for the last request for a given domain
        self.MIN_WAIT_SEC = 2          # The minimum number of seconds between every request to the same domain.
        self.numNewEntries = 0
        try:
            if compressed:                
                with lz4.frame.open(self.cachePath + '.lz4', 'rb') as f:
                    self.rcache = pickle.load(f)
                    print('Loaded compressed cache ({} entries).'.format(len(self.rcache)))
            else:
                with open(self.cachePath, 'rb') as f:
                    self.rcache = pickle.load(f)
                    print('Loaded request cache ({} entries).'.format(len(self.rcache)))
                
        except FileNotFoundError:
            pass
            
    def store(self, useCompression=True):
        if not self.numNewEntries:
            return
        
        if useCompression:
            with lz4.frame.open(self.cachePath + '.lz4', 'wb') as f:
                pickle.dump(self.rcache, f)
                print('Stored compressed request cache ({} entries).'.format(len(self.rcache)))
        else:
            with open(self.cachePath, 'wb') as f:
                pickle.dump(self.rcache, f)
                print('Stored request cache ({} entries).'.format(len(self.rcache)))
            
    def get_underlying_dict(self):
        return self.rcache
            
    def perform_request(self, url, artificialDelay=True, spoofHeaders=True):
        if artificialDelay:
            self.artificial_delay(url)
            
        print('Fetching {}...'.format(url))
        response = requests.Response()
        try:
            if spoofHeaders:
                headers = DEFAULT_HTTP_HEADERS_2
                # headers['Host'] = get_host(url)
                response = requests.get(url, headers=headers)
            else:
                response = requests.get(url)
        except requests.exceptions.InvalidSchema:
            print('[ERROR] Invalid schema for {}'.format(url))
            response.status_code = requests.codes.not_found
        except requests.exceptions.HTTPError:
            print('[ERROR] HTTP error')
        except requests.exceptions.ConnectionError:
            print('[ERROR] Connection error');
        except requests.exceptions.Timeout:
            print('[ERROR] Timeout')
        except requests.exceptions.TooManyRedirects:
            print('[ERROR] Too many redirects')

        # Only cache successful requests
        if response.status_code == requests.codes.ok:
            self.rcache[url] = response
            self.numNewEntries += 1
        else:
            print('Failed to retrive URL... skipping caching...')
        return response
            
    def get(self, url, useDelay=True, evictCache=False, cloakRequest=True):
        """ Drop-in replacement for requests.get, with added features (e.g. caching responses). """
        if url not in self.rcache or evictCache:
            return self.perform_request(url, artificialDelay=useDelay, spoofHeaders=cloakRequest)
        else:
            return self.rcache[url]
    
    def clear(self):
        self.rcache = {}
        
    def clear_entry(self, url):
        if url in rcache:
            del rcache[url]
        
    # very primitive wait time for requests:
    def artificial_delay(self, url):
        """ The purpose of this function is to reduce the number of requests per time. """
        host = get_host(url)
        if host in self.last_requests:
            timeSinceLastRequest = time.time() - self.last_requests[host]
            remainingWaitTime = max(0, (self.MIN_WAIT_SEC + random.random()) - timeSinceLastRequest)
            if remainingWaitTime > 0:
                time.sleep(remainingWaitTime)
        self.last_requests[host] = time.time()